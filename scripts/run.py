#!/usr/bin/env python3

import os
import sys
import subprocess
import xnat
import json
from datetime import datetime
import requests
from requests.auth import HTTPBasicAuth

def printErr(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
    print('\n')

def run_ts(vals):
    try:
        subprocess.run(vals,check=True)
    except subprocess.CalledProcessError as e:
        print('\n\nAn error has occurred when running Total Segmentator.\n\n')
        print('exit code: {}'.format(e.returncode))
        printErr(e)
        raise SystemExit(1)


def normalize_hostname(hostname):
    host = hostname.strip()
    if host.startswith("http://"):
        host = "https://" + host[len("http://"):]
    host = host.rstrip("/")
    return host

def login(host,username,password):
    basic = HTTPBasicAuth(username, password)
    response=requests.get(host+"/data/JSESSION", auth=basic)
    return response.content.decode("utf-8")


#login

host = os.environ['ENV_HOST']
login_user = os.environ['ENV_USER']
login_pass = os.environ['ENV_PSWD']
proj= os.environ['ENV_PROJ'] #has to be ID
expt = os.environ['ENV_EXPT'] #has to be ID
scan_id = os.environ['ENV_SCANID']

clean_host = normalize_hostname(host)
print('hostname is :' + str(clean_host))

jsessionid=login(clean_host,login_user,login_pass)

with xnat.connect(clean_host,detect_redirect=False,jsession=jsessionid) as xnat_session:
    experiment = xnat_session.projects[proj].experiments[expt]
    scan = experiment.scans[scan_id]
    modality = scan.modality
    if modality == 'MR':
        ts_task = 'total_mr'
    elif modality == 'CT':
        ts_task = 'total'
    else:
        print('Scan modality should be either CT or MR!')
        raise SystemExit(1)


# Run Total Segmentator as per any user defined args
list_args = sys.argv[1:]
if "-ot" in list_args:
    index = list_args.index("-ot")
    if (index + 1) < len(list_args) and list_args[index+1].lower() == 'dicom':
        list_args[index+1] = 'dicom'
    elif (index+1) < len(list_args) and list_args[index+1].lower() == 'nifti':
        list_args[index+1] = 'nifti'



command_to_run = ['TotalSegmentator'] + list_args
print(command_to_run)
run_ts(command_to_run)



if "-ot" in list_args:
    index = list_args.index("-ot")
    if (index + 1) < len(list_args) and list_args[index+1].lower() == 'dicom':
        output_folder_index = list_args.index("-o")
        output_folder_path = list_args[output_folder_index + 1]
        rtstruct_file_path = os.path.join(output_folder_path,'segmentations.dcm')
        try:
            jsessionid_renew = login(clean_host,login_user,login_pass)
            with xnat.connect(clean_host,detect_redirect=False,jsession=jsessionid_renew) as xnat_session:
                rtstruct_label = 'TS-RTStruct-' + ("{}{}".format(str(proj)[0:1],str(proj)[-1:])) + '-' + str(expt) + '-' + '-scan-' + str(scan_id) +'-'+ ("{}".format(datetime.now().strftime('%Y%m%d_%H%M%S')))
                target_url = (f"/xapi/roi/projects/{proj}/sessions/{expt}/collections/{rtstruct_label}"f"?type=RTSTRUCT&overwrite=true")
                with open(rtstruct_file_path, 'rb') as file:
                    response = xnat_session.put(target_url, data=file)
                    print(response)
        except:
            jsessionid_renew = login(clean_host,login_user,login_pass)
            with xnat.connect(clean_host,detect_redirect=False,jsession=jsessionid_renew) as xnat_session:
                rtstruct_label = 'TS-RTStruct-' + ("{}{}".format(str(proj)[0:1],str(proj)[-1:])) + '-' + str(expt) + '-' + '-scan-' + str(scan_id) +'-'+ ("{}".format(datetime.now().strftime('%Y%m%d_%H%M%S')))
                target_url = (f"/xapi/roi/projects/{proj}/sessions/{expt}/collections/{rtstruct_label}"f"?type=RTSTRUCT&overwrite=true")
                with open(rtstruct_file_path, 'rb') as file:
                    response = xnat_session.put(target_url, data=file)
                    print(response)
