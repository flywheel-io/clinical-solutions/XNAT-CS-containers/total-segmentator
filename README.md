# **TotalSegmentator** #

## v2.4.0: support for CT and MR

* XNAT containerized version of Total Segmentator v2.4.0

* This new **v2.4.0** can:
1.  go from DCM -> OHIF compatible TS RTStruct
2.  optionally generate stats (-s flag)
3.  support for MR segmentation. One of two options: Select MR tasks using `total_mr` for the `-ta` flag (in addition to CT which is the default option i.e `total`)


## v1.2: support for radiomics and statistics

* This is an improvement from the official TS v1.5.5 which generates radiomics only if both input and output type are NIFTI, but errors out if given DICOM inputs and/or if DICOM output format is selected

* This new **v1.5.5-customizedcontainer** can:
1.  go from DCM -> OHIF compatible TS RTStruct
2.  optionally generate stats (-s flag) and 32 radiomics features (-r flag) for each of the 104 segmentation masks.
3.  has minor bug fixes around dcm2niix downloads, explicitly sets memory requirements etc.

## USAGE :
* You may avail the new features by using **registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/total-segmentator:1.2**

* Please note that if you are building your own custom container using this repo, you would _need to edit your docker image's totalsegmentator installation_ and replace with the files provided in the _totalsegmentator_ folder in this repo. These changes are required for the radiomics and statistics workflow when going from DICOM inputs-> DICOM output,  and are not available via the official v1.5.5 of TS.
Easiest would be to just use the pre-built container that already has these changes
**registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/total-segmentator:1.2**

## v1.1: bug fix for TS generated RTStruct naming criteria (to ensure uniqueness)


## Overview

* This XNAT Container wraps a deep learning CT segmentation algorithm called TotalSegmentator (v1.5.5)

### Summary

* TotalSegmentator is a tool for segmentation of 104 classes in CT images.
* It was trained on a wide range of different CT images (different scanners,institutions,
protocols,...) and therefore should work well on most images.


### Cite

* Wasserthal J., Meyer M., Breit H., Cyriac J., Yang S., Segeroth M. TotalSegmentator: robust segmentation of 104 anatomical structures in CT images, 2022.	arXiv:2208.05868
[Arxiv-link](https://arxiv.org/abs/2208.05868)

TotalSegmentator is heavily based on nnUNet
* Isensee, F., Jaeger, P. F., Kohl, S. A., Petersen, J., & Maier-Hein, K. H. (2021). nnU-Net: a self-configuring
method for deep learning-based biomedical image segmentation. Nature methods, 18(2), 203-211.


License: *MIT*

## Usage

### Description
* This scan-level XNAT Container runs v1.5.5 as available here https://github.com/wasserth/TotalSegmentator/releases/tag/v1.5.5
* The input is a CT scan in DICOM format
* The default output is a DICOM RTStruct with the segmentation contours saved as a RTStruct (ROI Collection Assessment).
* Additionally the output DICOM file is also saved as a Scan resource in a folder named 'TotalSegmentator-Output'
* Alternately, the user can choose to save output in nifti format in which case the NIFTI masks corresponding to all 104    
  classes will be saved in the Scan resource folder named 'TotalSegmentator-Output'.
* The Container will run on GPU (recommended) or CPU (much slower; you may use --fast in 'other options' for faster runtime oN
  CPU at the cost of using a lower res model instead of the normal high-res model. By default , the container will run the high-res model)


### Logging

* Logging will indicate if the processing was run on a GPU or CPU.
* Three models are run to generate the final output.
* The processing time for each model will be displayed in the log.
